const path = require('path');
const express = require('express');
require('express-async-errors');
const morgan = require('morgan');
const dotenv = require('dotenv');
const hpp = require('hpp');
const xss = require('xss-clean');
const helmet = require('helmet');
const rateLimit = require('express-rate-limit');
const slowDown = require('express-slow-down');

dotenv.config({ path: './.env' });

const app = express();

app.use(helmet());

app.use(morgan('dev'));

// Body Parsing
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Security Middlewares
app.use(hpp());
app.use(xss());

// static directory
app.use(express.static(path.join(__dirname, '/public')));

app.get('/', (req, res, next) => {
  res.send('<center><h1>Welcome To Tasks App 😃</h1></center>');
});

// ROUTES
app.use('/api/v1/products', require('./routes/product.routes'));
app.use('/api/v1/users', require('./routes/user.routes'));

app.post('/api/v1/programs-missed-calls/', require('./controllers/misscallController'));

const rateLimiter = rateLimit({
  windowMs: 5 * 60 * 1000, // 5 minutes
  max: 20, // limit each IP to 10 requests per windowMs
  message: 'Too many requests, please try again after an hour'
});

const speedLimiter = slowDown({
  windowMs: 2 * 60 * 1000, // 2 minutes
  delayAfter: 10, // allow 10 requests per 2 minutes, then...
  delayMs: 500 // begin adding 500ms of delay per request above 10:
  // request # 11 is delayed by  500ms
  // request # 12 is delayed by 1000ms
  // and so on ...
});

app.use('/api/v1/voucher', speedLimiter, rateLimiter, require('./routes/voucher.routes'));

app.all('*', (req, res, next) => {
  res.status(404).send('Resource Not found');
});

app.use((err, req, res, next) => {
  res.status(err.statusCode || 500).json({
    status: 'fail',
    message: err.message
  });
});

module.exports = app;
