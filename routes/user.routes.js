const Joi = require('@hapi/joi');
const router = require('express').Router();
const AppError = require('../utils/AppError');
const { Sequelize } = require('sequelize');
const rateLimit = require('express-rate-limit');
const slowDown = require('express-slow-down');

const rateLimiter = rateLimit({
  windowMs: 5 * 60 * 1000, // 5 minutes
  max: 20, // limit each IP to 10 requests per windowMs
  message: 'Too many requests, please try again after an hour'
});

const speedLimiter = slowDown({
  windowMs: 2 * 60 * 1000, // 2 minutes
  delayAfter: 10, // allow 10 requests per 2 minutes, then...
  delayMs: 500 // begin adding 500ms delay
});

router.post('/blacklist', speedLimiter, rateLimiter, async (req, res, next) => {
  try {
    const schema = Joi.object({
      user: Joi.string()
        .min(3)
        .max(40)
        .pattern(new RegExp('^[a-zA-Z0-9]{3,40}$'))
        .messages({
          'string.pattern.base': 'Invalid user email or mobile'
        })
        .required()
    });

    const { error } = await schema.validate(req.body);
    if (error) return res.status(400).json({ status: 'fail', error: error.details[0].message });

    const { user } = req.body;

    const connection = await new Sequelize('testdb', 'root', 'devroot', {
      dialect: 'mysql',
      host: 'localhost'
    });

    const result = await connection.query(`SELECT * FROM blacklist_users WHERE user='${user}'`);
    console.log(result);
    if (result && result.length > 0 && result[0][0]) {
      return res.status(403).json({
        status: 'fail',
        error: `${result[0][0].user} is blocked, Please contact admin!`
      });
    }
    res.status(200).json({
      status: 'success',
      message: '200-OK'
    });
  } catch (err) {
    console.log(err);
    next(new AppError('Something went wrong', 500));
  }
});

module.exports = router;
