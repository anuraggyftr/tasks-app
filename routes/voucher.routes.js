const config = require('config');
const router = require('express').Router();
const axios = require('axios');
const AppError = require('../utils/AppError');

router.post('/', async (req, res, next) => {
  try {
    const { voucher } = req.body;
    const result = await axios({
      method: 'GET',
      url: `${config.get('STAGING_URL')}service/RestServiceImpl.svc/aQueryvoucher?userid=${config.get(
        'userId'
      )}&vouchernumber=${voucher}&password=${config.get('password')}`,
      headers: {
        'Content-Type': 'application/json'
      }
      // data: JSON.stringify({
      //   voucher: 'SOME-VOUCHER-CODE'
      // })
    });

    if (result && result.data) {
      return res.status(200).json({
        status: 'success',
        data: result.data
      });
    }
  } catch (err) {
    console.log(err);
    next(new AppError('Something went wrong', 500));
  }
});

module.exports = router;
