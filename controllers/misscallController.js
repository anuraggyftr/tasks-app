const config = require('config');
const { Sequelize } = require('sequelize');
const chalk = require('chalk');
const AppError = require('../utils/AppError');
const { sendSMS } = require('../controllers/smsController');
const { logger } = require('../utils/logUtility');

const miscallHandler = async (req, res, next) => {
  try {
    const whiteListedIPs = ['::1', '127.0.0.1', 'localhost'];

    const reqIP =
      req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket ? req.connection.socket.remoteAddress : null);

    if (!whiteListedIPs.includes(reqIP))
      return next(new AppError('Requested Blocked, Please contact Admin.', 403));

    const { db_name, db_host, db_user, db_pass } = config.get('db');
    const connection = await new Sequelize(db_name, db_user, db_pass, {
      dialect: 'mysql',
      host: db_host
    });

    /**
     * Implement Validations Later
     */

    const { program_name, mobile_number } = req.body;
    if (!program_name || !mobile_number)
      return next(new AppError('Please send program_name and mobile_number', 400));

    await connection.query(`CALL save_missedcalls('${program_name}', '${mobile_number}')`);

    // Send SMS
    const result = await sendSMS(mobile_number, config.get('sms').content);
    logger.info('SMS Result:', result);

    res.status(201).json({
      status: 'success',
      message: result.message,
      data: { saved: true }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      status: 'error',
      message: 'Internal Server Error.'
    });
  }
};

module.exports = miscallHandler;
