const axios = require('axios');
const config = require('config');
const chalk = require('chalk');
const { writeLog } = require('../utils/logUtility');

async function sendSMS(to, text) {
  const { SMS_API, FEED_ID, USERNAME, PASSWORD, SENDER_ID } = config.get('sms');
  const smsURL = `${SMS_API}?feedid=${encodeURIComponent(FEED_ID)}&username=${encodeURIComponent(
    USERNAME
  )}&password=${encodeURIComponent(PASSWORD)}&time&To=91${encodeURIComponent(to)}&Text=${encodeURIComponent(
    text
  )}&senderid=${encodeURIComponent(SENDER_ID)}`;

  try {
    const smsResponse = await axios.get(smsURL);
    console.log(chalk.bold.bgGreen('SMS RESPONSE'), smsResponse.data);
    if (smsResponse.status === 200 && smsResponse.data.includes('RESULT')) {
      writeLog('SMS', smsResponse.data);
      return { status: '1', message: 'sms sent successfully.' };
    }
    writeLog('SMS', 'smsResponse.data');
    return { status: '0', message: 'Error sending sms.' };
  } catch (err) {
    // console.log(err);
    err.Type = 'Send SMS ERROR';
    writeLog('SMS', JSON.stringify(err));
    return { status: '0', message: 'Error sending sms.' };
  }
}

module.exports = { sendSMS };
