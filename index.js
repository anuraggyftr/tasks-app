const http = require('http');
const app = require('./app');
//const sequelize = require('./utils/db');

const server = http.createServer(app);

// sequelize
//   .sync({ force: true })
//   .then(() => {
//     server.listen(3000, () =>
//       console.log(`server is ready at - http://localhost:3000 🚀`)
//     );
//   })
//   .catch((err) => console.log(err));

server.listen(3000, () => console.log(`server is ready at - http://localhost:3000 🚀`));
