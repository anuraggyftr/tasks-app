const fs = require('fs');
const path = require('path');
const winston = require('winston');

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.errors({ stack: true }),
    winston.format.splat(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.File({ filename: path.join(__dirname + './../logs/sms.log'), level: 'info' })
  ]
});

if (process.env.NODE_ENV === 'development') {
  logger.add(
    new transports.Console({
      format: format.combine(format.colorize(), format.simple()),
      level: 'info'
    })
  );
}

// OWN custom logger
const dateTimeFormat = function dateTimeFormat() {
  let now = new Date();
  let year = '' + now.getFullYear();
  let month = '' + (now.getMonth() + 1);
  if (month.length == 1) {
    month = '0' + month;
  }
  let day = '' + now.getDate();
  if (day.length == 1) {
    day = '0' + day;
  }
  let hour = '' + now.getHours();
  if (hour.length == 1) {
    hour = '0' + hour;
  }
  let minute = '' + now.getMinutes();
  if (minute.length == 1) {
    minute = '0' + minute;
  }
  let second = '' + now.getSeconds();
  if (second.length == 1) {
    second = '0' + second;
  }
  return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
};

const dateFormat = function dateTimeFormat() {
  let now = new Date();
  let year = '' + now.getFullYear();
  let month = '' + (now.getMonth() + 1);
  if (month.length == 1) {
    month = '0' + month;
  }
  day = '' + now.getDate();
  if (day.length == 1) {
    day = '0' + day;
  }
  return year + '-' + month + '-' + day;
};

function writeLog(type, log_str) {
  const fileName = dateFormat() + '.log';
  fs.appendFile('./logs/' + type + '/' + fileName, dateTimeFormat() + ':' + log_str + '\n', function (err) {
    if (err) {
      return console.log(err);
    }
    console.log('Log file saved.');
  });
}

module.exports = { logger, writeLog };
