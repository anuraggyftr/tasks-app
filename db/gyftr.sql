-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.20 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for gyftr
CREATE DATABASE IF NOT EXISTS `gyftr` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `gyftr`;

-- Dumping structure for table gyftr.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_guid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `expiry_date` date DEFAULT NULL,
  `available_qty` smallint NOT NULL,
  `status` enum('A','I') NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table gyftr.products: ~0 rows (approximately)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `product_guid`, `name`, `price`, `expiry_date`, `available_qty`, `status`, `created`, `updated`) VALUES
	(1, 'B829A455-8BB1-40C5-A7CE-3D7EB5457172', 'Croma INR 100', 100.00, '2021-12-31', 479, 'A', '2020-06-23 12:44:06', '2020-06-23 12:44:06'),
	(2, 'BDA28248-BE76-42F4-9A66-B239484B776E', 'Croma INR 500', 250.00, '2021-12-31', 330, 'A', '2020-06-23 12:44:35', '2020-06-23 12:44:35'),
	(3, 'B829A455-8BB1-40C5-A7CE-3D7EB5457174', 'Adidas Watch (TM) INR750', 750.00, '2020-12-31', 240, 'A', '2020-06-23 12:45:29', '2020-06-23 12:45:29');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
