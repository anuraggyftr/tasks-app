-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.20 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for client_db
CREATE DATABASE IF NOT EXISTS `client_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `client_db`;

-- Dumping structure for table client_db.clients
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int NOT NULL AUTO_INCREMENT,
  `buyer_user` varchar(100) NOT NULL,
  `buyer_password` varchar(255) NOT NULL,
  `db_user` varchar(100) NOT NULL,
  `db_password` varchar(100) NOT NULL,
  `db_name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table client_db.clients: ~3 rows (approximately)
DELETE FROM `clients`;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` (`id`, `buyer_user`, `buyer_password`, `db_user`, `db_password`, `db_name`, `created_at`, `updated_at`) VALUES
	(1, '3627544B-ED62-4483-A6EE-8C7BBA987308', '7757FE887C5', 'root', 'devroot', 'payback', '2020-06-22 13:10:03', '2020-06-22 13:10:03'),
	(2, '3627544B-ED62-4483-A6EE-8C7BBA987308', '7757FE887C5', 'anurag', '123456', 'gyftr', '2020-06-23 10:29:25', '2020-06-23 10:29:25'),
	(3, '3627544B-ED62-4483-A6EE-8C7BBA987308', '7757FE887C5', 'root', 'devroot', 'hdfc_smart_buy', '2020-06-23 10:31:37', '2020-06-23 10:31:37');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;

-- Dumping structure for procedure client_db.fetch_clients
DELIMITER //
CREATE PROCEDURE `fetch_clients`()
    COMMENT 'Fetches all clients'
BEGIN
	SELECT * FROM clients;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
